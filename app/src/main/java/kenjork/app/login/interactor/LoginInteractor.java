package kenjork.app.login.interactor;

/**
 * Created by kenyo on 23/11/2017.
 */

public interface LoginInteractor {
    void signIn(String username, String password);

}
