package kenjork.app.login.interactor;

import kenjork.app.login.presenter.LoginPresenter;
import kenjork.app.login.repository.LoginRepositorImpl;
import kenjork.app.login.repository.LoginRepository;

/**
 * Created by kenyo on 23/11/2017.
 */

public class LoginInteractorImpl implements LoginInteractor {

    private LoginPresenter loginPresenter;
    private LoginRepository repository;

    public LoginInteractorImpl(LoginPresenter loginPresenter) {
        this.loginPresenter = loginPresenter;
        repository = new LoginRepositorImpl(loginPresenter);
    }

    @Override
    public void signIn(String username, String password) {
        repository.singIm(username,password);
    }
}
