package kenjork.app.login.view;


public interface LoginView {

    void enableInputss();
    void disableInputs();

    void showProgressBar();
    void hideProgressBar();

    void loginError(String error);

    void goCreateAccount();
    void irHome();

}
