package kenjork.app.login.presenter;

import kenjork.app.login.interactor.LoginInteractor;
import kenjork.app.login.interactor.LoginInteractorImpl;
import kenjork.app.login.view.LoginView;

/**
 * Created by kenyo on 23/11/2017.
 */

public class LoginPresenterImpl implements LoginPresenter {

    private LoginView loginView;
    private LoginInteractor loginInteractor;

    public LoginPresenterImpl(LoginView loginView) {
        this.loginView = loginView;
        loginInteractor = new LoginInteractorImpl(this);
    }

    @Override
    public void signIn(String username, String password) {

        loginView.disableInputs();
        loginView.showProgressBar();

        loginInteractor.signIn(username,password);

    }

    @Override
    public void loginSuccess() {

        loginView.irHome();
        loginView.hideProgressBar();

    }

    @Override
    public void loginError(String error) {
        loginView.enableInputss();
        loginView.hideProgressBar();
        loginView.loginError(error);
    }
}
