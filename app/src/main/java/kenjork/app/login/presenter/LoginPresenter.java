package kenjork.app.login.presenter;

/**
 * Created by kenyo on 23/11/2017.
 */

public interface LoginPresenter {

    void signIn(String username,String password);
    void loginSuccess();
    void loginError(String error);

}
