package kenjork.app.post.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import kenjork.app.R;

public class NewPostActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_post);
    }
}
