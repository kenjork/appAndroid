package kenjork.app.post.view;


import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kenjork.app.R;
import kenjork.app.view.adapter.PicurteAdapterRecyclerView;
import kenjork.app.view.model.Picture;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    private static final int REQUEST_CAMARE = 1;
    private FloatingActionButton fabCamera;


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        showToolbar("Home", false, view);

        RecyclerView picturesRecycler  = (RecyclerView) view.findViewById(R.id.pictureRecycler);

        fabCamera = (FloatingActionButton) view.findViewById(R.id.fabCamera);


        LinearLayoutManager  linearLayoutManager =  new LinearLayoutManager(getContext());

        linearLayoutManager.setOrientation(linearLayoutManager.VERTICAL);

        picturesRecycler.setLayoutManager(linearLayoutManager);

        PicurteAdapterRecyclerView picurteAdapterRecyclerView = new PicurteAdapterRecyclerView(buidPictures(), R.layout.cardview_picture, getActivity());

        picturesRecycler.setAdapter(picurteAdapterRecyclerView);


        fabCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture();
            }
        });


        return view;
    }

    private void takePicture() {

        Intent intentTakePicture =  new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intentTakePicture.resolveActivity(getActivity().getPackageManager())!=null){
            startActivityForResult(intentTakePicture, REQUEST_CAMARE);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == REQUEST_CAMARE && resultCode == getActivity().RESULT_OK){
            Log.d("HomeFragment", "CAMERA OK!!  :) ");
        }


       // super.onActivityResult(requestCode, resultCode, data);
    }

    public ArrayList<Picture> buidPictures(){
        ArrayList<Picture> pictures = new ArrayList<>();
        pictures.add(new Picture("http://imagenesdepaisajes.net/wp-content/uploads/2016/08/alaska-imagenes-400x200.jpg", "Ana Lucia", "2 días" , "2 Me gustas"));
        pictures.add(new Picture("https://fotografodigital.com/wp-content/uploads/2017/07/Libro-Paisaje-Urbano-400x200.jpg", "Jose María", "5 días" , "8 Me gustas"));
        pictures.add(new Picture("http://imagenesdepaisajes.net/wp-content/uploads/2016/10/kauai-3-400x200.jpg", "Maria Lamia", "9 días" , "10 Me gustas"));

        return pictures;
    }



    public void showToolbar(String tittle, boolean upButton, View view){

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(tittle);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);

    }


}
